import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { Route } from './Route.entity';

@Entity('Schedule')
export class Schedule {
    @PrimaryGeneratedColumn()
    Schedule_id: number;

    @Column()
    Route_id: number;

    @Column()
    Time_start: string;

    @Column()
    Reverse: boolean;

    @ManyToOne(() => Route, route => route.schedules)
    @JoinColumn({ name: 'Route_id' })
    route: Route;
}
