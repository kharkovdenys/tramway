import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Type_card } from './Type_card.entity';

@Entity({ name: 'Price' })
export class Price {
    @PrimaryGeneratedColumn()
    Price_id: number;

    @ManyToOne(() => Type_card, { eager: true })
    @JoinColumn({ name: 'Type_card_id' })
    Type_card: Type_card;

    @Column()
    From: number;

    @Column()
    To: number;

    @Column({ type: 'decimal', precision: 8, scale: 2 })
    Price: number;
}
