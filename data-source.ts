import { DataSource } from "typeorm";
import { Route } from "./entities/Route.entity";
import { Schedule } from "./entities/Schedule.entity";
import { Travelcard } from "./entities/Travelcard.entity";
import { Type_card } from "./entities/Type_card.entity";
import { Users } from "./entities/Users.entity";
import { RouteStops } from "./entities/RouteStops.entity";
import { Stop } from "./entities/Stop.entity";
import { TravelHistory } from "./entities/TravelHistory.entity";
import { Price } from "./entities/Price.entity";
import { config } from "./config";

export const AppDataSource = new DataSource({
    type: "mssql",
    host: "localhost",
    database: "Tramway",
    port: 1433,
    username: config.username,
    password: config.password,
    options: {
        trustServerCertificate: true,
        encrypt: false
    },
    synchronize: false,
    logging: false,
    entities: [Route, Schedule, Travelcard, Type_card, Users, RouteStops, Stop, TravelHistory, Price],
    subscribers: [],
    migrations: [],
})
