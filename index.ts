import express from 'express';
import { urlencoded } from 'body-parser';
import { AppDataSource } from './data-source';
import { addSchedule, addUser, deleteSchedule, getRoutes, getScheduleByRoute, getType_cards, stops, useQR, useTravelcard } from './services/db';

const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

app.use(urlencoded({ extended: false }));

app.listen(port, async () => {
    await AppDataSource.initialize();
    console.log(`Server is running on port ${port}`);
});

app.get('/', (req, res) => {
    res.render('index');
});

app.get('/new/user', async (req, res) => {
    const result = await getType_cards();
    res.render('new_user', { result: result });
});

app.post('/new/user/result', async (req, res) => {
    const result = await addUser(parseInt(req.body.type), req.body.first_name, req.body.last_name, req.body.ipn);
    res.render('new_user_result', { result: result });
});

app.get('/qr', async (req, res) => {
    const result = await stops();
    res.render('qr', { result: result, text: '' });
});

app.post('/qr', async (req, res) => {
    var text = 'Успішно застосовано';
    try {
        await useQR(req.body.qr, parseInt(req.body.stop_id), req.body.reverse === 'true' ? true : false);
    } catch (e: any) {
        text = e.originalError.info.message;
    }
    const result = await stops();
    res.render('qr', { result: result, text: text });
});

app.get('/travelcard', async (req, res) => {
    const result = await stops();
    res.render('travelcard', { result: result, text: '' });
});

app.post('/travelcard', async (req, res) => {
    var text = 'Успішно застосовано';
    try {
        await useTravelcard(parseInt(req.body.card), parseInt(req.body.stop_id), req.body.reverse === 'true' ? true : false);
    } catch (e: any) {
        if (e.originalError === undefined) {
            text = e;
        }
        else {
            text = e.originalError.info.message;
        }
    }
    const result = await stops();
    res.render('travelcard', { result: result, text: text });
});

app.get('/routelist', async (req, res) => {
    const result = await getRoutes();
    res.render('routelist', { result: result });
});

app.get('/schedule/:id', async (req, res) => {
    const result = await getScheduleByRoute(parseInt(req.params.id));
    res.render('schedulelist', { result: result, id: parseInt(req.params.id) });
});

app.post('/schedule/:id', async (req, res) => {
    await deleteSchedule(parseInt(req.body.id));
    const result = await getScheduleByRoute(parseInt(req.params.id));
    res.render('schedulelist', { result: result, id: parseInt(req.params.id) });
});

app.get('/schedule/add/:id', async (req, res) => {
    res.render('addschedule');
});

app.post('/schedule/add/:id', async (req, res) => {
    await addSchedule(parseInt(req.params.id), req.body.time, req.body.reverse === 'true' ? true : false);
    res.redirect(`/schedule/${req.params.id}`);
});
