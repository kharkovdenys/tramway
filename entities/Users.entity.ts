import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, OneToOne } from 'typeorm';
import { Travelcard } from './Travelcard.entity';

@Entity({ name: 'Users' })
export class Users {
    @PrimaryGeneratedColumn()
    User_id: number;

    @Column()
    Username: string;

    @Column()
    Password: string;

    @Column()
    Travelcard_id: number;

    @OneToOne(() => Travelcard, travelcard => travelcard.users)
    @JoinColumn({ name: 'Travelcard_id' })
    travelcard: Travelcard;
}
