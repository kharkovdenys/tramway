import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Schedule } from './Schedule.entity';

@Entity({ name: 'Route' })
export class Route {
    @PrimaryGeneratedColumn()
    Route_id: number;

    @Column()
    Route_number: string;

    @Column()
    Duration: number;

    @Column()
    Stops: number;

    @OneToMany(() => Schedule, schedule => schedule.route)
    schedules: Schedule[];
}
