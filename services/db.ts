import { AppDataSource } from "../data-source";
import { Price } from "../entities/Price.entity";
import { Route } from "../entities/Route.entity";
import { RouteStops } from "../entities/RouteStops.entity";
import { Schedule } from "../entities/Schedule.entity";
import { TravelHistory } from "../entities/TravelHistory.entity";
import { Travelcard } from "../entities/Travelcard.entity";
import { Type_card } from "../entities/Type_card.entity";
import { Users } from "../entities/Users.entity";
import { generateRandomPassword, generateUsername, hashPassword } from "./auth";

export async function getRoutes(): Promise<Route[]> {
    return AppDataSource.getRepository(Route).find();
}

export async function getScheduleByRoute(RouteID: number): Promise<Schedule[]> {
    return AppDataSource.getRepository(Schedule).find({ where: { Route_id: RouteID }, order: { Time_start: 'ASC' } });
}

export async function updateScheduleById(Schedule_id: number, Route_id: number, Time_start: string, Reverse: boolean): Promise<void> {
    const schedule = await AppDataSource.getRepository(Schedule).findOneBy({ Schedule_id });
    if (schedule) {
        schedule.Route_id = Route_id;
        schedule.Time_start = Time_start;
        schedule.Reverse = Reverse;
        await AppDataSource.getRepository(Schedule).save(schedule);
    }
}

export async function addSchedule(Route_id: number, Time_start: string, Reverse: boolean): Promise<void> {
    const schedule = new Schedule();
    schedule.Route_id = Route_id;
    schedule.Time_start = Time_start;
    schedule.Reverse = Reverse;
    await AppDataSource.getRepository(Schedule).save(schedule);
}

export async function deleteSchedule(id: number): Promise<void> {
    await AppDataSource.getRepository(Schedule).delete({ Schedule_id: id });
}

export async function getType_cards(): Promise<Type_card[]> {
    return AppDataSource.getRepository(Type_card).find();
}

export async function addUser(Type_card_id: number, First_name: string, Last_name: string, IPN: string): Promise<{ 'username': string, 'password': string }> {
    const username = generateUsername(First_name, Last_name);
    const password = generateRandomPassword();

    await AppDataSource.transaction(async (transactionalEntityManager) => {
        const travelcard = new Travelcard();
        travelcard.Type_card_id = Type_card_id;
        travelcard.First_name = First_name;
        travelcard.Last_name = Last_name;
        travelcard.IPN = IPN;
        travelcard.Balance = 0;
        const newTravelcard = await transactionalEntityManager.getRepository(Travelcard).save(travelcard);
        const user = new Users();
        user.Travelcard_id = newTravelcard.Travelcard_id;
        user.Username = username;
        user.Password = hashPassword(password);
        await transactionalEntityManager.getRepository(Users).save(user);
    });
    return { username, password };
}

export async function useQR(uid: string, stop_id: number, reverse: boolean) {
    await AppDataSource.manager.query('EXEC useQR @0, @1, @2', [uid, stop_id, reverse]);
}

export async function useTravelcard(uid: number, stop_id: number, reverse: boolean) {

    await AppDataSource.transaction(async (transactionalEntityManager) => {
        const routeEnd = await transactionalEntityManager.getRepository(RouteStops).findOne({ where: { Route_stops_id: stop_id } });
        if (routeEnd === null) {
            throw Error("Немає такої зупинки");
        }
        const travelcard = await transactionalEntityManager.getRepository(Travelcard).findOne({ where: { Travelcard_id: uid } });
        if (travelcard === null) {
            throw Error("Немає такої картки");
        }
        const prices = await transactionalEntityManager.getRepository(Price).find({ where: { Type_card: { Type_card_id: travelcard.Type_card_id } } });
        const lastRoute = await transactionalEntityManager.getRepository(RouteStops).findOne({ where: { Route_id: routeEnd.Route_id }, order: { Stop_number: "DESC" } });
        if (lastRoute === null) {
            throw Error("Немає останьої зупинки");
        }
        const travelhistory = await transactionalEntityManager.getRepository(TravelHistory).findOne({ where: { Travelcard_id: uid }, order: { Date_of_start: "DESC" }, relations: { routeStart: true } });
        if (travelhistory !== null && travelhistory.Reverse === reverse && travelhistory.Route_end_id === null && travelhistory.routeStart.Route_id === routeEnd.Route_id) {
            var total_price = 0;
            var startRoute = routeEnd.Stop_number;
            var endRoute = lastRoute.Stop_number - 1;
            if (reverse) {
                var startRoute = travelhistory.routeStart.Stop_number - routeEnd.Stop_number + 1;
                var endRoute = travelhistory.routeStart.Stop_number - 1;
            }
            for (var price of prices) {
                if (price.From <= startRoute && price.To >= endRoute) {
                    total_price += price.Price;
                    break;
                } else if (price.From <= startRoute && price.To >= startRoute) {
                    total_price += (price.To - startRoute + 1) * price.Price;
                } else if (price.From >= startRoute && price.To <= endRoute) {
                    total_price += (price.To - price.From + 1) * price.Price;
                } else if (price.From <= endRoute && price.To >= endRoute) {
                    total_price += (endRoute - price.From + 1) * price.Price;
                    break;
                }
            }
            if (endRoute < startRoute) {
                var total_price = 0;
            }
            await transactionalEntityManager.query(
                'UPDATE Travelcard SET Balance = Balance + @0 WHERE Travelcard_id = @1',
                [total_price, travelcard.Travelcard_id]
            );
            travelhistory.Route_end_id = routeEnd.Route_stops_id;
            await transactionalEntityManager.getRepository(TravelHistory).save(travelhistory);
            await transactionalEntityManager.query('INSERT INTO [Transaction] VALUES(@0, 7, @1, @2)', [uid, new Date(Date.now()), total_price]);
            return;
        }
        if (travelcard.Balance < 0) {
            throw Error("Баланс від'ємний");
        }
        var total_price = 0;
        var startRoute = 1;
        var endRoute = lastRoute.Stop_number - routeEnd.Stop_number;
        if (reverse) {
            var endRoute = routeEnd.Stop_number - 1;
        }

        for (var price of prices) {
            if (price.From <= startRoute && price.To >= endRoute) {
                total_price += price.Price;
                break;
            } else if (price.From <= startRoute && price.To >= startRoute) {
                total_price += (price.To - startRoute + 1) * price.Price;
            } else if (price.From >= startRoute && price.To <= endRoute) {
                total_price += (price.To - price.From + 1) * price.Price;
            } else if (price.From <= endRoute && price.To >= endRoute) {
                total_price += (endRoute - price.From + 1) * price.Price;
                break;
            }
        }
        if (endRoute === 0) {
            var total_price = 0;
        }
        await transactionalEntityManager.query(
            'UPDATE Travelcard SET Balance = Balance - @0 WHERE Travelcard_id = @1',
            [total_price, travelcard.Travelcard_id]
        );
        const newTravelHistory = new TravelHistory();
        newTravelHistory.Date_of_start = new Date(Date.now());
        newTravelHistory.Reverse = reverse;
        newTravelHistory.Route_start_id = stop_id;
        newTravelHistory.Travelcard_id = uid;
        await transactionalEntityManager.getRepository(TravelHistory).save(newTravelHistory);
        await transactionalEntityManager.query('INSERT INTO [Transaction] VALUES(@0, 7, @1, @2)', [uid, newTravelHistory.Date_of_start, -total_price]);
    });
}

export async function stops() {
    return AppDataSource.getRepository(RouteStops).find({ relations: { stop: true } });
}
